package com.hcl.dao;

import java.sql.ResultSet;

import java.sql.SQLException;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Repository;

import com.hcl.bean.Books;

@Repository
public class BooksDao {
	@Autowired
	JdbcTemplate jdbcTemplate;
	public List<Books> getAllBooks(){
		try {
			return jdbcTemplate.query("select * from books",new bookRowMapper());
		} catch (Exception exception) {
			System.out.println(" Restoring books"+exception);
			return null;
		}
		
		
	}
	public int storeLikedBooks(Books books,String user) {
		try {
			return jdbcTemplate.update("insert into likedbooks values(?,?,?)",books.getBookId(),books.getTitle(),
					books.getGenre());
		} catch (Exception exception) {
			System.out.println(" Liked books"+exception);
			return 0;
		}
	}
	public int storeReadLaterBooks(Books books,String user) {
		try {
			return jdbcTemplate.update("insert into readlaterbooks values(?,?,?)",books.getBookId(),books.getTitle(),
					books.getGenre(),books);
		} catch (Exception exception) {
			System.out.println(" Readlater books"+exception);
			return 0;
		}
	}
	public List<Books> getAllLikedBooks(String user){
		try {
			return jdbcTemplate.query("select bookId,title,genre from likedbooks where user=?",new likedBookRowMapper(),user);
		} catch (Exception e) {
			System.out.println("GetlikedBooks"+e);
			return null;
		}

}
	public List<Books> getAllReadLaterBooks(String user){
		try {
			return jdbcTemplate.query("select bookId,title,genre from readlaterbooks where user=?",new readLaterBookRowMapper(),user);
		} catch (Exception e) {
			System.out.println("GetlikedBooks"+e);
			return null;
		}

}
}
class bookRowMapper implements RowMapper<Books>{

	@Override
	public Books mapRow(ResultSet resultSet, int rowNum) throws SQLException {
		Books book=new Books();
		book.setBookId(resultSet.getInt(1));
		book.setTitle(resultSet.getString(2));
		book.setGenre(resultSet.getString(3));
		
		return book;
	}
	
}
class likedBookRowMapper implements RowMapper<Books>{

	@Override
	public Books mapRow(ResultSet resultSet, int rowNum) throws SQLException {
		Books book=new Books();
		book.setBookId(resultSet.getInt(1));
		book.setTitle(resultSet.getString(2));
		book.setGenre(resultSet.getString(3));
		
		return book;
	}
	
}
class readLaterBookRowMapper implements RowMapper<Books>{

	@Override
	public Books mapRow(ResultSet resultSet, int rowNum) throws SQLException {
		Books book=new Books();
		book.setBookId(resultSet.getInt(1));
		book.setTitle(resultSet.getString(2));
		book.setGenre(resultSet.getString(3));
		
		return book;
	}
	
}


