package com.hcl.controllers;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import com.hcl.bean.Login;
import com.hcl.service.LoginService;

@Controller
public class LoginController {
	@Autowired
	LoginService loginService;
	@RequestMapping(value="welcome",method=RequestMethod.GET)
	public ModelAndView welcomepage() {
		ModelAndView modelAndView=new ModelAndView();
		modelAndView.setViewName("login.jsp");
		return modelAndView;
	}
	@RequestMapping(value="Register",method=RequestMethod.GET)
	public ModelAndView Registrationpage() {
		ModelAndView modelAndView=new ModelAndView();
		modelAndView.setViewName("registration.jsp");
		return modelAndView;
	}
	@RequestMapping(value="Registration",method=RequestMethod.POST)
	public ModelAndView StoreRegistrationDetails(HttpServletRequest httpRequest) {
		ModelAndView modelAndView=new ModelAndView();
				
		String email=httpRequest.getParameter("email");
		String password=httpRequest.getParameter("pass");
		
		Login login=new Login();
		login.setEmail(email);
		login.setPassword(password);
	
		String result=loginService.storeRegistrationInfo(login);
		if(result.equalsIgnoreCase("Success")) {
			httpRequest.setAttribute("loginMessage","Successfully Registered");
			modelAndView.setViewName("login.jsp");
			
		}else {
			httpRequest.setAttribute("registrationMessage","Sorry!..Registration Failed");
			modelAndView.setViewName("index.jsp");	
		}			
		return modelAndView;
	}
	@RequestMapping(value="login",method=RequestMethod.GET)
	public ModelAndView checkLoginDetails(HttpServletRequest servletRequest,HttpSession httpSession) {
		ModelAndView modelAndView=new ModelAndView();
		String email=servletRequest.getParameter("email");
		String pass=servletRequest.getParameter("password");
		String user=email.substring(0, email.indexOf('@'));
		Login login=new Login();
		login.setEmail(email);
		login.setPassword(pass);
	
		String res=loginService.checkLoginInfo(email);
		if(res.equalsIgnoreCase(pass)) {
			httpSession.setAttribute("user",user);		
			modelAndView.setViewName("dashboard.jsp");
		}else {
			servletRequest.setAttribute("flog","give details properly");
			modelAndView.setViewName("index.jsp");
		}
		
		
		return modelAndView;
		
	}
	

}

