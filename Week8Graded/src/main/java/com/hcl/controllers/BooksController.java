package com.hcl.controllers;

import java.util.Iterator;

import java.util.List;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import com.hcl.bean.Books;
import com.hcl.service.BooksService;

@Controller
public class BooksController {
	@Autowired
	BooksService booksService;
	List<Books>listOfBooks;
	@RequestMapping(value="display",method=RequestMethod.GET)
	public ModelAndView displayAllBooks(HttpServletRequest servletReq,HttpSession httpSession) {
		ModelAndView modelAndView=new ModelAndView();
		listOfBooks=booksService.fetchAllBooks();	
		httpSession.setAttribute("obj", listOfBooks);
		modelAndView.setViewName("display.jsp");
		return modelAndView;
		
	}
	@RequestMapping(value="Dashboard",method=RequestMethod.GET)
	public ModelAndView displayBooksForUser(HttpSession httpSession) {
		ModelAndView modelAndView=new ModelAndView();
		listOfBooks=booksService.fetchAllBooks();	
		httpSession.setAttribute("obj1", listOfBooks);
		modelAndView.setViewName("Welcome.jsp");
		return modelAndView;

	}
	@RequestMapping(value="like",method=RequestMethod.POST)
	public ModelAndView listOfBooks(HttpServletRequest servletReq,HttpSession httpSession) {
		ModelAndView modelAndView=new ModelAndView();
		if(servletReq.getParameter("Like")!=null) {
			int bookId=Integer.parseInt(servletReq.getParameter("bookId"));
		Iterator<Books>iterator=listOfBooks.iterator();
		while(iterator.hasNext()) {
			Books book=iterator.next();
			if(bookId==book.getBookId()) {
				Object user=httpSession.getAttribute("user");
				String res=booksService.storeLikedBooksInfo(book, user.toString());
				modelAndView.setViewName("Welcome.jsp");
				
			}
			
		}
		}
		return modelAndView;		
	}
	@RequestMapping(value="readlater",method=RequestMethod.POST)
	public ModelAndView listOfReadLaterBooks(HttpServletRequest servletReq,HttpSession httpSession) {
		ModelAndView modelAndView=new ModelAndView();
		if(servletReq.getParameter("readlater")!=null) {
			int bookId=Integer.parseInt(servletReq.getParameter("bookId"));
		Iterator<Books>iterator=listOfBooks.iterator();
		while(iterator.hasNext()) {
			Books book=iterator.next();
			if(bookId==book.getBookId()) {
				Object user=httpSession.getAttribute("user");
				String res=booksService.storeReadLaterBooksInfo(book, user.toString());
				modelAndView.setViewName("Welcome.jsp");
				
			}
			
		}
		}
		return modelAndView;
	}
	@RequestMapping(value="likedbooks",method=RequestMethod.GET)
	public ModelAndView displayLikedBooksForUser(HttpSession httpSession) {
		ModelAndView modelAndView=new ModelAndView();
		Object user=httpSession.getAttribute("user");
		listOfBooks=booksService.fetchAllLikedBooks(user.toString());	
		httpSession.setAttribute("obj2", listOfBooks);
		System.out.println(listOfBooks);
		modelAndView.setViewName("likedbooks.jsp");
		return modelAndView;

	}
	@RequestMapping(value="readlaterbooks",method=RequestMethod.GET)
	public ModelAndView displayReadLaterBooksForUser(HttpSession httpSession) {
		ModelAndView modelAndView=new ModelAndView();
		Object user=httpSession.getAttribute("user");
		listOfBooks=booksService.fetchAllReadLikedBooks(user.toString());	
		httpSession.setAttribute("obj3", listOfBooks);
		System.out.println(listOfBooks);
		modelAndView.setViewName("readlater.jsp");
		return modelAndView;

	}
	
	
	@RequestMapping(value="logout")
	public ModelAndView logout() {
		ModelAndView modelAndView=new ModelAndView();
		modelAndView.setViewName("logout.jsp");
		return modelAndView;
	}
	
}