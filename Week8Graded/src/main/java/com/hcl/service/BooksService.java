package com.hcl.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.hcl.bean.Books;
import com.hcl.dao.BooksDao;

@Service
public class BooksService {
	@Autowired
	BooksDao booksDao;
	public List<Books> fetchAllBooks(){
		return booksDao.getAllBooks();
	}
	public String storeLikedBooksInfo(Books book,String user) {
		if(booksDao.storeLikedBooks(book, user)>0) {
			return "Liked books are stored Sucessfully ";
		}else {
			return "Failed to store likedbooks";
		}
	}
	public String storeReadLaterBooksInfo(Books book,String user) {
		if(booksDao.storeReadLaterBooks(book, user)>0) {
			return " ReadLater books are stored Sucessfully ";
		}else {
			return "Failed to store readlater books";
		}
	}
	public List<Books> fetchAllLikedBooks(String user){
		return booksDao.getAllLikedBooks(user);
	}
	public List<Books> fetchAllReadLikedBooks(String user){
		return booksDao.getAllReadLaterBooks(user);
	}
	
 
}

