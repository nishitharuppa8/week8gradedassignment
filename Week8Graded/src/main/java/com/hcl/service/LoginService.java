package com.hcl.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.hcl.bean.Login;
import com.hcl.dao.LoginDao;

@Service
public class LoginService {
	@Autowired
    LoginDao loginDao;
	public String storeRegistrationInfo(Login login) {
		if(loginDao.StoreRegistrationDetails(login)>0) {
			return "Successfully logged in";
			
		}else {
			return "Failed to Login";
		}
	}
	
	public String checkLoginInfo(String email) {
		return loginDao.checkLoginDetails(email);
		
		
	}

}